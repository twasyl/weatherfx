/*
 * Copyright 2014 Thierry Wasylczenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.twasyl.weatherfx.beans;

import javafx.beans.property.*;
import javafx.scene.image.Image;

/**
 * @author Thierry Wasylczenko
 */
public class Weather {

    private final IntegerProperty weatherCode = new SimpleIntegerProperty();
    private final StringProperty city = new SimpleStringProperty();
    private final DoubleProperty temperature = new SimpleDoubleProperty();
    private final DoubleProperty minTemperature = new SimpleDoubleProperty();
    private final DoubleProperty maxTemperature = new SimpleDoubleProperty();
    private final StringProperty weatherCondition = new SimpleStringProperty();
    private final ObjectProperty<Image> icon = new SimpleObjectProperty<Image>();

    public IntegerProperty weatherCodeProperty() { return weatherCode; }
    public int getWeatherCode() { return weatherCode.get(); }
    public void setWeatherCode(int weatherCode) { this.weatherCode.set(weatherCode); }

    public StringProperty cityProperty() { return city; }
    public String getCity() { return city.get(); }
    public void setCity(String city) { this.city.set(city); }

    public DoubleProperty temperatureProperty() { return temperature; }
    public double getTemperature() { return temperature.get(); }
    public void setTemperature(double temperature) { this.temperature.set(temperature); }

    public DoubleProperty minTemperatureProperty() { return minTemperature; }
    public double getMinTemperature() { return minTemperature.get(); }
    public void setMinTemperature(double minTemperature) { this.minTemperature.set(minTemperature); }

    public DoubleProperty maxTemperatureProperty() { return maxTemperature; }
    public double getMaxTemperature() { return maxTemperature.get(); }
    public void setMaxTemperature(double maxTemperature) { this.maxTemperature.set(maxTemperature); }

    public StringProperty weatherConditionProperty() { return weatherCondition; }
    public String getWeatherCondition() { return weatherCondition.get(); }
    public void setWeatherCondition(String weatherCondition) { this.weatherCondition.set(weatherCondition); }

    public ObjectProperty<Image> iconProperty() { return icon; }
    public Image getIcon() { return icon.get(); }
    public void setIcon(Image icon) { this.icon.set(icon); }
}
