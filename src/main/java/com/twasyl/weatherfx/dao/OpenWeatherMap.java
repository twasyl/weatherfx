/*
 * Copyright 2014 Thierry Wasylczenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.twasyl.weatherfx.dao;

import com.oracle.javafx.jmx.json.JSONDocument;
import com.oracle.javafx.jmx.json.JSONFactory;
import com.oracle.javafx.jmx.json.JSONReader;
import com.twasyl.weatherfx.beans.Weather;
import javafx.css.PseudoClass;
import javafx.scene.image.Image;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.*;

/**
 * @author Thierry Wasylczenko
 */
public class OpenWeatherMap implements IWeatherDAO {

    private static double KELVIN_TO_CELSIUS = 273.15;

    private String getWeatherJSON(String city) {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final HttpClient client = new DefaultHttpClient();
        final HttpGet request = new HttpGet("http://api.openweathermap.org/data/2.5/weather?q=" + city);

        try {
            final HttpResponse response = client.execute(request);
            final InputStream responseStream = response.getEntity().getContent();
            final byte[] buffer = new byte[1024];
            int byteRead = 0;

            while((byteRead = responseStream.read(buffer)) != -1) {
                out.write(buffer, 0, byteRead);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            return out.toString("UTF-8");
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    @Override
    public Weather getWeather(String city) {
        final String jsonString = this.getWeatherJSON(city);
        final Weather weather = jsonString == null ? null : new Weather();

        if(weather != null) {

            final JSONReader reader = JSONFactory.instance()
                    .makeReader(new InputStreamReader(
                                    new ByteArrayInputStream(jsonString.getBytes()))
                    );

            final JSONDocument document = reader.build();


            // City
            weather.setCity(document.getString("name"));

            // Check the weather condition
            final JSONDocument weatherCondition = (JSONDocument) document.getList("weather").get(0);
            weather.setWeatherCondition(weatherCondition.getString("description"));

            // Get the weather code
            weather.setWeatherCode(weatherCondition.getNumber("id").intValue());

            // Icon
            final Image icon = new Image(String.format("http://openweathermap.org/img/w/%1$s.png", weatherCondition.getString("icon")));
            weather.setIcon(icon);

            // Check the temperatures
            final JSONDocument temperatures = document.get("main");
            weather.setTemperature(temperatures.getNumber("temp").doubleValue() - KELVIN_TO_CELSIUS);
            weather.setMinTemperature(temperatures.getNumber("temp_min").doubleValue() - KELVIN_TO_CELSIUS);
            weather.setMaxTemperature(temperatures.getNumber("temp_max").doubleValue() - KELVIN_TO_CELSIUS);
        }

        return weather;
    }
}
