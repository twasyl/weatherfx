/*
 * Copyright 2014 Thierry Wasylczenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.twasyl.weatherfx.controls;

import com.twasyl.weatherfx.beans.Weather;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.text.Text;
import javafx.util.Callback;


/**
 * @author Thierry Wasylczenko
 */
public class TemperatureCellFactory implements Callback<TableColumn<Weather,Double>,TableCell<Weather,Double>> {

    @Override
    public TableCell<Weather, Double> call(TableColumn<Weather, Double> param) {
        final TableCell<Weather, Double> cell = new TableCell<Weather, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {

                if(!empty && item != null) {
                    final Text text = new Text(String.format("%1$.2f °C", item));
                    setGraphic(text);
                } else {
                    setGraphic(null);
                }
            }
        };

        return cell;
    }
}
