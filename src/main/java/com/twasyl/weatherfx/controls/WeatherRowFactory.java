/*
 * Copyright 2014 Thierry Wasylczenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.twasyl.weatherfx.controls;

import com.twasyl.weatherfx.beans.Weather;
import javafx.beans.property.IntegerProperty;
import javafx.css.PseudoClass;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.util.Callback;

/**
 * @author Thierry Wasylczenko
 */
public class WeatherRowFactory implements Callback<TableView<Weather>, TableRow<Weather>> {

    @Override
    public TableRow<Weather> call(TableView<Weather> param) {
        final TableRow<Weather> row = new TableRow<>();

        row.itemProperty().addListener((value, oldCity, newCity) -> {
            // Remove old PseudoClass
            if(oldCity != null) {
                final PseudoClass oldPseudoClass = determinePseudoClass(oldCity.getWeatherCode());
                row.pseudoClassStateChanged(oldPseudoClass, false);
            }

            if(newCity == null) row.setTooltip(null);
            else {
                final StringBuilder builder = new StringBuilder()
                        .append("City: ").append(row.getItem().getCity()).append("\n")
                        .append("Weather code: ").append(row.getItem().getWeatherCode());

                final Tooltip tooltip = new Tooltip(builder.toString());
                row.setTooltip(tooltip);

                // Set the PseudoClass according the code
                final PseudoClass newPseudoClass = determinePseudoClass(newCity.getWeatherCode());
                row.pseudoClassStateChanged(newPseudoClass, true);
            }
        });

        return row;
    }


    private PseudoClass determinePseudoClass(int weatherCode) {
        String suffix;

        if(weatherCode >= 200 && weatherCode <= 232) suffix = "thunderstorm";
        else if(weatherCode >= 300 && weatherCode <= 321) suffix = "drizzle";
        else if(weatherCode >= 500 && weatherCode <= 531) suffix = "rain";
        else if(weatherCode >= 600 && weatherCode <= 622) suffix = "snow";
        else if(weatherCode >= 701 && weatherCode <= 781) suffix = "atmosphere";
        else if(weatherCode >= 800 && weatherCode <= 804) suffix = "clouds";
        else if(weatherCode >= 900 && weatherCode <= 906) suffix = "extreme";
        else if(weatherCode >= 951 && weatherCode <= 962) suffix = "additional";
        else suffix = "unknown";

        return PseudoClass.getPseudoClass("weather-".concat(suffix));
    }
}
