/*
 * Copyright 2014 Thierry Wasylczenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.twasyl.weatherfx.app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * @author Thierry Wasylczenko
 */
public class WeatherFX extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        final Pane root = FXMLLoader.load(WeatherFX.class.getResource("/com/twasyl/weatherfx/fxml/WeatherFX.fxml"));

        final Scene scene = new Scene(root);

        primaryStage.setTitle("WeatherFX");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        WeatherFX.launch(args);
    }
}
