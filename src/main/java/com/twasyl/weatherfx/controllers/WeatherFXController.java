/*
 * Copyright 2014 Thierry Wasylczenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.twasyl.weatherfx.controllers;

import com.twasyl.weatherfx.beans.Weather;
import com.twasyl.weatherfx.dao.IWeatherDAO;
import com.twasyl.weatherfx.dao.OpenWeatherMap;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * @author Thierry Wasylczenko
 */
public class WeatherFXController implements Initializable {

    @FXML private TextField cityName;
    @FXML private TableView<Weather> cities;

    private IWeatherDAO weatherDAO = new OpenWeatherMap();

    @FXML private void addCity(ActionEvent event) {
        this.cities.getItems().add(this.weatherDAO.getWeather(this.cityName.getText()));
        this.cityName.setText(null);
    }

    @FXML private void reloadAll(ActionEvent event) {
        this.cities.getItems().forEach(city -> this.weatherDAO.getWeather(city.getCity()));
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
